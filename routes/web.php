<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/drivers', 'DriverController@index')->name('drivers');
Route::get('/assessments', 'AssessmentController@index')->name('assessments');
Route::get('/trainings', 'TrainingController@index')->name('trainings');
Route::get('/questionnaires', 'DriverQuestionnaireController@index')->name('questionnaires');
