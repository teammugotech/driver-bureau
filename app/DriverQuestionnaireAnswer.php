<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverQuestionnaireAnswer extends Model
{
    protected $connection = 'mysql';
    protected $table = 'driver_questionnaire_answers';
    protected $guarded = [];
}
