<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    protected $connection = 'mysql';
    protected $table = 'assessments';
    protected $guarded = [];
}

