<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverTraining extends Model
{
    protected $connection = 'mysql';
    protected $table = 'driver_trainings';
    protected $guarded = [];
}
