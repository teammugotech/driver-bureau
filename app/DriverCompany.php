<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverCompany extends Model
{
    protected $connection = 'mysql';
    protected $table = 'driver_companies';
    protected $guarded = [];
}
