<?php

namespace App\Http\Controllers;

use App\Driver;
use App\DriverTraining;
use App\Training;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TrainingController extends Controller
{

    /**
     * Script to transfer driver trainings to new consolidated trainings & driver_trainings tables
     */
    public function index(){
        ini_set('max_execution_time', '-1');
        $companies = DB::connection('db')->select("select co_id from dib_companies ORDER BY co_id");
        foreach ($companies as $company){
            if (DB::connection('rs')->select("SHOW TABLES LIKE 'rsd_egt".$company->co_id."'")){
                $driverTrainings = DB::connection('rs')
                    ->select("
                            SELECT t.*, d.`dl_idno`, d.`dl_passportno`, d.`dl_finalvpjstep`
                            FROM rsd_egt$company->co_id t
                            INNER JOIN rs_dl$company->co_id d
                            ON t.`egt_dlid` = d.`dl_id`
                    ");
                if ($driverTrainings){
                    $this->createTrainings($driverTrainings, $company);
                }
            }
        }

        dd('Done Importing Root Storage Trainings....!');
    }

    /**
     * @param $driverTrainings
     * @param $company
     */
    public function createTrainings($driverTrainings, $company){
        foreach ($driverTrainings as $driverTraining){
            $id = !empty($driverTraining->dl_idno) ? $driverTraining->dl_idno : $driverTraining->dl_passportno;
            $driver = Driver::where('id_number', $id)
                ->orWhere('employee_number', $id)
                ->first();

            if ($driver){
                $training = new Training();
                $training->driver_id = $driver->id;
                $training->company_id = $company->co_id;
                $training->year = Carbon::parse($driverTraining->egt_year . '-01-01');
                $training->total = $driverTraining->egt_total;
                $training->created_at = Carbon::parse($driverTraining->egt_year . '-01-01');
                $training->updated_at = Carbon::parse($driverTraining->egt_lastupdate);

                if ($training->save()){
                    $this->createDriverTraining($training, $driverTraining, '-01-01', $driverTraining->egt_jan);
                    $this->createDriverTraining($training, $driverTraining, '-02-01', $driverTraining->egt_feb);
                    $this->createDriverTraining($training, $driverTraining, '-03-01', $driverTraining->egt_mar);
                    $this->createDriverTraining($training, $driverTraining, '-04-01', $driverTraining->egt_apr);
                    $this->createDriverTraining($training, $driverTraining, '-05-01', $driverTraining->egt_may);
                    $this->createDriverTraining($training, $driverTraining, '-06-01', $driverTraining->egt_jun);
                    $this->createDriverTraining($training, $driverTraining, '-07-01', $driverTraining->egt_jul);
                    $this->createDriverTraining($training, $driverTraining, '-08-01', $driverTraining->egt_aug);
                    $this->createDriverTraining($training, $driverTraining, '-09-01', $driverTraining->egt_sep);
                    $this->createDriverTraining($training, $driverTraining, '-10-01', $driverTraining->egt_oct);
                    $this->createDriverTraining($training, $driverTraining, '-11-01', $driverTraining->egt_nov);
                    $this->createDriverTraining($training, $driverTraining, '-12-01', $driverTraining->egt_dec);
                }
            } else{
                Log::notice('rsd_egt'.$company->co_id.' Driver not found in new database', (array) $driverTraining);
            }
        }
    }

    /**
     * @param $training
     * @param $driverTraining
     * @param $monthDate
     * @param $score
     */
    public function createDriverTraining($training, $driverTraining, $monthDate, $score){
        $dTraining = new DriverTraining();
        $dTraining->training_id = $training->id;
        $dTraining->month = Carbon::parse($driverTraining->egt_year . $monthDate);
        $dTraining->score = $score;
        $dTraining->step = $driverTraining->dl_finalvpjstep;
        $dTraining->created_at = Carbon::parse($driverTraining->egt_year . $monthDate);
        $dTraining->updated_at = Carbon::parse($driverTraining->egt_year . $monthDate);
        $dTraining->save();
    }
}
