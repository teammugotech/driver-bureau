<?php

namespace App\Http\Controllers;

use App\Driver; // Driver Eloquent Model
use App\DriverCompany; // DriverCompany Eloquent Model
use App\DriverUpload; // DriverUpload Eloquent Model
use Carbon\Carbon;
use Illuminate\Support\Facades\DB; // Database Manager Class

class DriverController extends Controller
{

    /**
     * DriverController constructor.
     */
    public function __construct(){
        //$this->middleware('auth');
    }

    /**
     * Script to transfer drivers to new table
     */
    public function index(){
        ini_set('max_execution_time', '-1'); // Set max exec timer off to avoid the script from breaking
        $companies = DB::connection('db')->select("select co_id from dib_companies ORDER BY co_id"); // Get all company ids from Driver Bureau database DB::connection('db')
        foreach ($companies as $company){
            // Check if each company table exist before querying all the company drivers from RootStorage database DB::connection('rs')
            if (DB::connection('rs')->select("SHOW TABLES LIKE 'rs_dl".$company->co_id."'")){
                $users = DB::connection('rs')->select("select * from rs_dl$company->co_id"); // Select all drivers from company table
                if ($users){
                    // Create drivers locally if users is not empty / null
                    $this->createDrivers($users);
                }
            }
        }

        dd('Done Importing Root Storage Drivers....!'); // Laravel Die and Dump function like var_dump($array)
    }

    /**
     * Create Drivers
     * @param $users
     */
    public function createDrivers($users){
        foreach ($users as $user){
            $identifier = !empty($user->dl_idno) ? $user->dl_idno : $user->dl_passportno;
            $userExists = Driver::where('id_number', $identifier)
                ->orWhere('employee_number', $identifier)
                ->first();

            if ($userExists){
                // The driver with same id or passport number has been imported already from another company
                // So just check if the current $user record is latest than the created one and update accordingly
                if(Carbon::parse($userExists->created_at)->greaterThan(Carbon::parse($user->dl_firstupdate))){
                    $userExists->updated_at = Carbon::parse($user->dl_latestupdate);
                    $userExists->save();
                }
                if(Carbon::parse($userExists->updated_at)->lessThan(Carbon::parse($user->dl_latestupdate))){
                    $userExists->updated_at = Carbon::parse($user->dl_latestupdate);
                    $userExists->save();
                }

                // Assign this $driver to a company and also create driver upload entries
                $this->createDriverCompanyEntry($userExists->id, $user);
                $this->createDriverUploadEntry($userExists->id, $user);
            } else {
                // The driver does not exist in our new tables, so lets save the driver
                $driver = new Driver();
                $driver->name = $user->dl_firstname;
                $driver->surname = $user->dl_surname;
                $driver->nationality = $user->dl_nationality;
                $driver->id_number = $user->dl_idno;
                $driver->employee_number = $user->dl_passportno;
                $driver->cell_number = $user->dl_cellno;
                $driver->created_at = Carbon::parse($user->dl_firstupdate);
                $driver->updated_at = Carbon::parse($user->dl_latestupdate);
                $driver->save();

                // Assign this $driver to a company and also create driver upload entries
                $this->createDriverCompanyEntry($driver->id, $user);
                $this->createDriverUploadEntry($driver->id, $user);
            }
        }
    }

    /** Create Driver Upload Entry
     * @param $driverId
     * @param $user
     */
    public function createDriverUploadEntry($driverId, $user){
        $uploads = explode(',', $user->dl_updated);
        foreach ($uploads as $upload){
            $driverUpload = new DriverUpload();
            $driverUpload->driver_id = $driverId;
            $driverUpload->company_id = $user->dl_coid;
            $driverUpload->step = $user->dl_finalvpjstep;
            $driverUpload->final_assessment_date = ($user->dl_egfinalassess == '0000-00-00' || $user->dl_egfinalassess == '') ? NULL : Carbon::parse($user->dl_egfinalassess);
            $driverUpload->created_at = Carbon::parse($upload);
            $driverUpload->updated_at = Carbon::parse($upload);
            $driverUpload->save();
        }
    }

    /**
     * Create Driver Company Entry
     * @param $driverId
     * @param $user
     */
    public function createDriverCompanyEntry($driverId, $user){
        $driverCompany = new DriverCompany();
        $driverCompany->driver_id = $driverId;
        $driverCompany->company_id = $user->dl_coid;
        $driverCompany->position = $user->dl_job;
        $driverCompany->department = $user->dl_dept;
        $driverCompany->industry = $user->dl_industry;
        $driverCompany->training_venue = !empty($user->dl_trainvenue) ? $user->dl_trainvenue : 'g';
        $driverCompany->eyegym_send = ($user->dl_eyegymsend == 'y') ? 1 : 0;
        $driverCompany->eyegym_registered = ($user->dl_eyegymreg == 'y') ? 1 : 0;
        $driverCompany->eyegym_expiry_date = ($user->dl_eyegymexpire == 'Never used' || $user->dl_eyegymexpire == '') ? NULL : Carbon::parse($user->dl_eyegymexpire);
        $driverCompany->final_assessment_date = ($user->dl_egfinalassess == '0000-00-00' || $user->dl_egfinalassess == '') ? NULL : Carbon::parse($user->dl_egfinalassess);
        $driverCompany->step = $user->dl_finalvpjstep;
        $driverCompany->last_activity = ($user->dl_lastactivity == '0000-00-00' || $user->dl_lastactivity == '') ? NULL : Carbon::parse($user->dl_lastactivity);
        $driverCompany->status = $user->dl_status;
        $driverCompany->login_date = ($user->dl_firstlogin == '0000-00-00' || $user->dl_firstlogin == '') ? NULL : Carbon::parse($user->dl_firstlogin);
        $driverCompany->history = $user->dl_vpjhistory;
        $driverCompany->created_at = Carbon::parse($user->dl_firstupdate);
        $driverCompany->updated_at = Carbon::parse($user->dl_latestupdate);
        $driverCompany->save();
    }
}
