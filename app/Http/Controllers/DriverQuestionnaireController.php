<?php

namespace App\Http\Controllers;

use App\Driver;
use App\DriverQuestionnaire;
use App\DriverQuestionnaireAnswer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DriverQuestionnaireController extends Controller
{

    private $questionnaire;
    private $qnrAnswer;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        DriverQuestionnaire $questionnaire,
        DriverQuestionnaireAnswer $qnrAnswer
    )
    {
        $this->questionnaire = $questionnaire;
        $this->qnrAnswer = $qnrAnswer;
    }

    /**
     * Script to transfer driver questionnaire to new consolidated driver_questionnaires & driver questionnaire_answers tables
     */
    public function index(){
        ini_set('max_execution_time', '-1');
        $companies = DB::connection('db')->select("select co_id from dib_companies ORDER BY co_id");
        foreach ($companies as $company){
            if (DB::connection('rs')->select("SHOW TABLES LIKE 'rsd_dq".$company->co_id."'")){
                $driverQuestionnaires = DB::connection('rs')
                    ->select("
                            SELECT dq.*, d.`dl_idno`, d.`dl_passportno`
                            FROM rsd_dq$company->co_id dq
                            INNER JOIN rs_dl$company->co_id d
                            ON dq.`rq_dlid` = d.`dl_id`
                    ");
                if ($driverQuestionnaires){
                    $this->createQuestionnaires($driverQuestionnaires, $company);
                }
            }
        }
        dd('Done Importing Root Storage Questionnaires....!');
    }

    public function createQuestionnaires($driverQuestionnaires, $company){
        foreach ($driverQuestionnaires as $driverQuestionnaire){
            $id = !empty($driverQuestionnaire->dl_idno) ? $driverQuestionnaire->dl_idno : $driverQuestionnaire->dl_passportno;
            $driver = Driver::where('id_number', $id)
                ->orWhere('employee_number', $id)
                ->first();

            if ($driver) {
                if(
                    $this->questionnaire->where([
                        'driver_id' => $driver->id,
                        'company_id' => $company->co_id,
                        'questionnaire_id' => $driverQuestionnaire->rq_qnaireid,
                        'eyegym_compare' => $driverQuestionnaire->rq_egacompare,
                        'created_at' => date('Y-m-d h:i:s', $driverQuestionnaire->rq_datetime)
                    ])->exists()
                ){
                    Log::info('Record exist for driver id :'. $driver->id, []);
                    continue;
                }

                /** Create | Save driver questionnaire entry */
                $questionnaire = new $this->questionnaire;
                $questionnaire->driver_id = $driver->id;
                $questionnaire->company_id = $company->co_id;
                $questionnaire->questionnaire_id = $driverQuestionnaire->rq_qnaireid;
                $questionnaire->eyegym_compare = $driverQuestionnaire->rq_egacompare;
                $questionnaire->created_at = date('Y-m-d h:i:s', $driverQuestionnaire->rq_datetime);
                $questionnaire->updated_at = Carbon::parse($driverQuestionnaire->rq_qdate)->format('Y-m-d h:i:s');
                $questionnaire->save();

                /** Query driver questionnaire_answer from RS prod database */
                if ($questionnaire->id){
                    if (DB::connection('rs')->select("SHOW TABLES LIKE 'rsd_dqa".$company->co_id."'")){
                        $driverQuestionnaireAnswers = DB::connection('rs')
                            ->select("
                                SELECT dqa.*
                                FROM rsd_dqa$company->co_id dqa
                                WHERE dqa.`rqa_dqid` = $driverQuestionnaire->rq_id
                                ORDER BY dqa.`rqa_id`
                            ");

                        if ($driverQuestionnaireAnswers){
                            $this->createQuestionnaireAnswers($driverQuestionnaireAnswers, $questionnaire);
                        }
                    } else {
                        Log::notice('rsd_dqa'.$company->co_id.' table not found in RS prod database', (array) $company);
                    }
                }
            } else {
                Log::notice('rsd_dq'.$company->co_id.' Driver not found in new database', (array) $driverQuestionnaire);
            }

        }
    }

    /**
    * Create | Save driver questionnaire_answer entries
    */
    public function createQuestionnaireAnswers($driverQuestionnaireAnswers, $questionnaire) {
        foreach ($driverQuestionnaireAnswers as $driverQuestionnaireAnswer){
            $driverAnswer = new $this->qnrAnswer;
            $driverAnswer->driver_questionnaire_id = $questionnaire->id;
            $driverAnswer->question_set_id = $driverQuestionnaireAnswer->rqa_qsid;
            $driverAnswer->question_id = $driverQuestionnaireAnswer->rqa_qid;
            $driverAnswer->yes_no = $driverQuestionnaireAnswer->rqa_yesno;
            $driverAnswer->average = $driverQuestionnaireAnswer->rqa_average;
            $driverAnswer->stddev = $driverQuestionnaireAnswer->rqa_stddev;
            $driverAnswer->text_checkbox = $driverQuestionnaireAnswer->rqa_text_checkbox;
            $driverAnswer->save();
        }
    }
}
