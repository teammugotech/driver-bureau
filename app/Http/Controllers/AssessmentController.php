<?php

namespace App\Http\Controllers;

use App\Assessment;
use App\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AssessmentController extends Controller
{
    /**
     * Script to transfer driver assessments to new consolidated table
     */
    public function index(){
        ini_set('max_execution_time', '-1');
        $companies = DB::connection('db')->select("select co_id from dib_companies ORDER BY co_id");
        foreach ($companies as $company){
            if (DB::connection('rs')->select("SHOW TABLES LIKE 'rsd_ega".$company->co_id."'")){
                $driverAssessments = DB::connection('rs')
                    ->select("
                            SELECT a.*, d.`dl_idno`, d.`dl_passportno`, d.`dl_finalvpjstep`
                            FROM rsd_ega$company->co_id a
                            INNER JOIN rs_dl$company->co_id d
                            ON a.`ega_dlid` = d.`dl_id`
                    ");
                if ($driverAssessments){
                    $this->createAssessments($driverAssessments, $company);
                }
            }
        }

        dd('Done Importing Root Storage Assessments....!'); // Laravel Die and Dump function like var_dump($array)
    }

    /**
     * @param $driverAssessments
     * @param $company
     */
    public function createAssessments($driverAssessments, $company){
        foreach ($driverAssessments as $driverAssessment){
            $id = !empty($driverAssessment->dl_idno) ? $driverAssessment->dl_idno : $driverAssessment->dl_passportno;
            $driver = Driver::where('id_number', $id)
                ->orWhere('employee_number', $id)
                ->first();

            if($driver){
                $assessment = new Assessment();
                $assessment->driver_id = $driver->id;
                $assessment->company_id = $company->co_id;
                $assessment->step = $driverAssessment->dl_finalvpjstep;
                $assessment->assessment_date = $driverAssessment->ega_date;
                $assessment->completed_date = $driverAssessment->ega_assessdate;
                $assessment->eye_jumps_coordination = $driverAssessment->ega_eyejumps;
                $assessment->eye_hand_reaction = $driverAssessment->ega_eyehand;
                $assessment->peripheral_response = $driverAssessment->ega_peripheral;
                $assessment->visual_concentration = $driverAssessment->ega_tracking;
                $assessment->visual_recognition = $driverAssessment->ega_recognition;
                $assessment->overall = $driverAssessment->ega_overall;
                $assessment->save();
            } else{
                Log::notice('rsd_ega'.$company->co_id.' Driver not found in new database', (array) $driverAssessment);
            }
        }
    }
}

