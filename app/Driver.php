<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $connection = 'mysql';
    protected $table = 'drivers';
    protected $guarded = [];
}
