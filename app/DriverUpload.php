<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverUpload extends Model
{
    protected $connection = 'mysql';
    protected $table = 'driver_uploads';
    protected $guarded = [];
}
