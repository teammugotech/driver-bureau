<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverQuestionnaire extends Model
{
    protected $connection = 'mysql';
    protected $table = 'driver_questionnaires';
    protected $guarded = [];
}
