<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $connection = 'mysql';
    protected $table = 'trainings';
    protected $guarded = [];
}
