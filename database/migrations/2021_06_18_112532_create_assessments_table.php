<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('driver_id');
            $table->unsignedBigInteger('company_id');
            $table->integer('step');
            $table->dateTime('assessment_date');
            $table->dateTime('completed_date');
            $table->unsignedInteger('eye_jumps_coordination')->default(0);
            $table->unsignedInteger('eye_hand_reaction')->default(0);
            $table->unsignedInteger('peripheral_response')->default(0);
            $table->unsignedInteger('visual_concentration')->default(0);
            $table->unsignedInteger('visual_recognition')->default(0);
            $table->unsignedInteger('overall')->default(0);
            $table->timestamps();
            $table->foreign('driver_id')
                ->references('id')
                ->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
}
