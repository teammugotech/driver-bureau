<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('driver_id');
            $table->unsignedBigInteger('company_id');
            $table->string('position')->nullable();
            $table->string('department')->nullable();
            $table->integer('step')->unsigned();
            $table->string('industry')->nullable();
            $table->enum('training_venue', ['g', 'c', 'i'])->default('g');
            $table->boolean('eyegym_send')->default(1);
            $table->boolean('eyegym_registered')->default(0);
            $table->dateTime('eyegym_expiry_date')->nullable();
            $table->dateTime('final_assessment_date')->nullable();
            $table->dateTime('login_date')->nullable();
            $table->dateTime('last_activity')->nullable();
            $table->enum('status', ['e', 'r', 'u', 'x'])->default('e');
            $table->longText('history')->nullable();
            $table->timestamps();
            $table->index(['driver_id', 'company_id']);
            $table->foreign('driver_id')
                ->references('id')
                ->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_companies');
    }
}
