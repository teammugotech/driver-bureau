<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverQuestionnaireSetAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_questionnaire_answers', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->unsignedBigInteger('driver_questionnaire_id')->index();
            $table->unsignedBigInteger('question_set_id')->index();
            $table->unsignedBigInteger('question_id')->index();
            $table->decimal('yes_no', 5, 2);
            $table->decimal('average', 5, 2);
            $table->decimal('stddev', 5, 2);
            $table->string('text_checkbox')->nullable();
            $table->timestamps();

            $table->foreign('driver_questionnaire_id')
                ->references('id')
                ->on('driver_questionnaires');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_questionnaire_answers');
    }
}
